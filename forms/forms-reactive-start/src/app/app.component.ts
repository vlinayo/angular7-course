import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  genders = ['male', 'female'];
  signUpForm: FormGroup;
  forbiddennUsername= ['Chris', 'Anna'];

  ngOnInit(){
    this.signUpForm = new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]),
        'email': new FormControl(null, [Validators.required, Validators.email],this.forbiddenEmails)
      }),      
      'gender': new FormControl('female'),
      'hobbies': new FormArray([])
    });
    //show every value change on form
    // this.signUpForm.valueChanges.subscribe(
    //   (value) => console.log(value)
    // );
    //verify form status
    // this.signUpForm.statusChanges.subscribe(
    //   (status) => console.log(status)
    // );
    this.signUpForm.setValue({
      'userData': {
        'username': 'Max',
        'email': 'max@test.com'
      },
      'gender': 'male',
      'hobbies': []
    });

    this.signUpForm.patchValue({
      'userData': {
        'username': 'Anna'
      }
    })

  }

  onSubmit(){
    console.log(this.signUpForm );
    this.signUpForm.reset();
    
  }

  onAddHobby(){
    const control = new FormControl(null, Validators.required);
    (<FormArray> this.signUpForm.get('hobbies')).
      push(control);
  }

  forbiddenNames(control: FormControl): {[s: string]:boolean} {
    if(this.forbiddennUsername.indexOf(control.value) !== -1) {
      return {'nameIsFobbiden': true};
    }else{
      return null; //when validation is succesfull!
    }
  }

  forbiddenEmails(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>((resolve,reject) => {
      setTimeout(() => {
        if(control.value === 'test@test.com'){
          resolve ({'emailIsForbidden': true});
        }else{
          resolve(null);
        }
      }, 1500);
    });
    return promise;

  }

}
