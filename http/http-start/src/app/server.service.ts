import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
// import { Observable } from "rxjs/Observable";
// import 'rxjs/Rx';
import { catchError, map } from "rxjs/operators";
import { throwError } from "rxjs";

@Injectable()
export class ServerService{

    constructor(private http: Http){

    }

    storeServers(servers: any[]){
        //si necesitas ajustar los headers del request
        const header = new Headers({'Content-Type':'applications/json'});
        
        // usar POST para hacer UPDATES!! NO PARA CREATE/OVERWRITE
        // return this.http.post('https://udemy-ng-http-68079.firebaseio.com/data.json', servers, 
        //     {headers: header});

        //PUT para CREAR
        return this.http.put('https://udemy-ng-http-68079.firebaseio.com/data.json', servers, 
            {headers: header});
    }

    getServers(){
        return this.http.get('https://udemy-ng-http-68079.firebaseio.com/')
        .pipe(
            map(
            (response: Response) => {
                const data = response.json();
                for (const server of data){
                    server.name = 'Fetched_' + server.name;
                }
                return data;
            }
        )).pipe(catchError(
            (error: Response) => {
                // console.log(error);
                return throwError('Something went wrong');
            }
        ));
    }

    getAppName(){
        return this.http.get('https://udemy-ng-http-68079.firebaseio.com/appName.json')
        .pipe(
            map(
               (response: Response) => {
                   return response.json();
               }
            ));
    }
}