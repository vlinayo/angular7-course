import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  odds: number[] = [];
  evens: number[] = [];
  totalOdds: number = 0;
  totalEvens: number = 0;

  onIntervalEmit(firedNum: number){
    console.log(firedNum)
    if(firedNum % 2 == 0){
      this.evens.push(firedNum);
      this.totalEvens = this.totalEvens +1;
    }else{
      this.odds.push(firedNum);
      this.totalOdds= this.totalOdds +1;
    }
  
  }

}


