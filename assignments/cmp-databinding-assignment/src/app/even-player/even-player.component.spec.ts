import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvenPlayerComponent } from './even-player.component';

describe('EvenPlayerComponent', () => {
  let component: EvenPlayerComponent;
  let fixture: ComponentFixture<EvenPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvenPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvenPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
