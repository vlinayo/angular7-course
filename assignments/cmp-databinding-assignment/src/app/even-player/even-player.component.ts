import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-even-player',
  templateUrl: './even-player.component.html',
  styleUrls: ['./even-player.component.css']
})
export class EvenPlayerComponent implements OnInit {
  @Input() evenNum: number;

  constructor() { }

  ngOnInit() {
  }

}
