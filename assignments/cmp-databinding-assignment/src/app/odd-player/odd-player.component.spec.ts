import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OddPlayerComponent } from './odd-player.component';

describe('OddPlayerComponent', () => {
  let component: OddPlayerComponent;
  let fixture: ComponentFixture<OddPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OddPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OddPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
