import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-odd-player',
  templateUrl: './odd-player.component.html',
  styleUrls: ['./odd-player.component.css']
})
export class OddPlayerComponent implements OnInit {
  @Input() oddNum :number;

  constructor() { }

  ngOnInit() {
  }

}
