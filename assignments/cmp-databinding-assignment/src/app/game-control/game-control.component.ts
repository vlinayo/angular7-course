import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  num: number = 0;
  interval;
  @Output() intervalEmited= new EventEmitter<number>();
  
  constructor() { }

  ngOnInit() {
  }

  onStartGame(){
    this.interval = setInterval(() => {
      this.num = Math.floor(Math.random() * 100) + 1;
      this.intervalEmited.emit(this.num)
    }, 1000);
  }

  onStopGame(){
    clearInterval(this.interval);
  }



}
