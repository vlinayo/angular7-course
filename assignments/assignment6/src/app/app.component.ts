import { Component, ViewChild, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'assignment6';
  @ViewChild('f') myForm: NgForm; 
  subscriptions = ['Basic','Advanced', 'Pro'];
  submitted= false;
  selectedSubscription = 'Advanced';
  user={
    email: '',
    subscription: '',
    passwrod: ''
  }
    

  onSubmit(){
    console.log(this.myForm);
    this.submitted=true;
    this.user.email= this.myForm.value.email;
    this.user.subscription= this.myForm.value.subscription;
    this.myForm.reset();
  }
}
