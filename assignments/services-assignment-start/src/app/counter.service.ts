export class counterService {
    inactiveActions = 0;
    activeActions = 0;

    addToInactive(){
        this.inactiveActions = this.inactiveActions +1;
        console.log("Total inactive actions: "+ this.inactiveActions);
    }

    addToActive(){
        this.activeActions = this.activeActions +1;
        console.log("Total active actions: "+ this.activeActions);
    }
}