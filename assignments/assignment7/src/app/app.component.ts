import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  newProject: FormGroup;

  ngOnInit(){
    this.newProject = new FormGroup({
      'projectName': new FormControl(null, [Validators.required], this.forbiddenNames),
      'email': new FormControl(null, [Validators.required, Validators.email]),      
      'status': new FormControl('stable')
    });
  }

  onSubmit(){
    console.log(this.newProject.value);
    // this.newProject.reset();
  }

  forbiddenNames(control: FormControl): Promise<any> | Observable<any> {
    const promise= new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if(control.value === 'Test') {
          resolve ({'nameIsForbidden' : true});
        }else{
          resolve(null);
        }
      }, 2000);
    });
    return promise;
  }


}
