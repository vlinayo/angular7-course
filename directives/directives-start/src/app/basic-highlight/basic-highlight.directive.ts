import { Directive, ElementRef } from '@angular/core';

@Directive({
    selector: '[appHighlight]' //asi se reconoce sin tener que usar los []
})


export class BasicHighlightDirective{
    constructor(private elementRef : ElementRef){

    }

    ngOnInit(){
        this.elementRef.nativeElement.style.backgroundColor = 'green';
    }
}
