import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detail-logger',
  templateUrl: './detail-logger.component.html',
  styleUrls: ['./detail-logger.component.css']
})
export class DetailLoggerComponent implements OnInit {
  displayDetail=false;
  clicks = [];
  numberOfLogs = 0;
  timestamps = [];
  time = new Date();

  constructor() { 
  }

  ngOnInit() {
  }

  onDisplayDetails(){
    this.displayDetail=!this.displayDetail;
    this.clicks.push(this.numberOfLogs++);
    this.timestamps.push(this.time.getTime());
  }

  getColor(){
    if(this.clicks.length >4){
      return 'blue';
    }else{
      return 'white';
    }
  }

}
