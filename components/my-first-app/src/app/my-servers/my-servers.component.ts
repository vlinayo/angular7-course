import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-servers',
  templateUrl: './my-servers.component.html',
  styleUrls: ['./my-servers.component.css']
})
export class MyServersComponent implements OnInit {
  servers = [];

  constructor() { }

  ngOnInit() {
  }

  onAddServer(){
    this.servers.push('Another Server');
  }

  onRemoveServer(id: number){
    const position = id;
    this.servers.splice(position, 1);
  }

}
