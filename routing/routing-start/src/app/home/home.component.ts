import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router,
    private authSerice: AuthService) { }

  ngOnInit() {
  }

  onLoadServers(id : number){
    //do something before loading servers..
    this.router.navigate(['/servers', id, 'edit'], {queryParams: {allowEdit: '1'}, fragment:"loading" });

  }

  onLogin() {
    this.authSerice.login();
  }

  onLogout(){
    this.authSerice.logout();
  }

}
