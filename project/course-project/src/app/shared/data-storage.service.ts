import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { recipeService } from "../recipe-book/recipe.service";
import { Recipe } from "../recipe-book/recipe.model";
import { map } from "rxjs/operators";
import { AuthService } from "../auth/auth.service";

@Injectable()
export class DataStorageService {
    constructor(private http: Http,
        private recipeService: recipeService,
        private authService: AuthService){

    }

    storeRecipes(){
        const token = this.authService.getToken();

        return this.http.put('https://udemy-ng-project-a07e2.firebaseio.com/recipes.json?auth='+token,
        this.recipeService.getRecipes());
    }

    fetchRecipes(){
        const token = this.authService.getToken();

        this.http.get('https://udemy-ng-project-a07e2.firebaseio.com/recipes.json?auth='+token)
        .pipe(
            map(
                (response: Response) => {
                    const recipes: Recipe[] = response.json();
                    for ( let recipe of recipes){
                        if(!recipe['ingredients']){
                            console.log(recipe);
                            recipe['ingredients'] = [];
                        }
                    }
                    return recipes;
                }
            ))
        .subscribe(
            (recipes: Recipe[]) => {
                // const recipes: Recipe[] = response.json();
                this.recipeService.setRecipes(recipes);
            }
        );
        
    }
}