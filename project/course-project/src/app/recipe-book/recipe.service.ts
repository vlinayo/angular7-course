import { Recipe } from './recipe.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.module';
import { shoppingListService } from '../shopping/shopping-list.service';
import { Subject } from 'rxjs';

@Injectable()
export class recipeService{
    recipesChanged = new Subject<Recipe[]>();

    // recipeSelected = new EventEmitter<Recipe>();

    private recipes: Recipe[]= [
        new Recipe('Pizza!',
        'This is a pizza recipe',
        'https://img.bestrecipes.com.au/RCK3slSo/h300-w400-cscale/br-api/asset/20771/super-easy-pizza-dough-recipe.jpg',
        [
            new Ingredient('Cheeese',5),
            new Ingredient('Tomatoes',6),
            new Ingredient('Flour',8)
        ]),

        new Recipe('Tortilla de Patatas',
        'Asi se hace!',
        'https://cdn1.cocina-familiar.com/recetas/thumb/tortilla-de-patata-con-cebolla.jpg',
        [
            new Ingredient('Potatoes', 4),
            new Ingredient('Onions', 1.5),
            new Ingredient('Eggs', 6)
        ])
    ];

    constructor(private slService: shoppingListService){}

    setRecipes(recipes: Recipe[]){
        this.recipes = recipes;
        this.recipesChanged.next(this.recipes.slice());
    }

    getRecipes(){
        return this.recipes.slice(); //returns new array that is exact copy from recipe
    }

    addIngredientToShoppingList(ingredients: Ingredient[]){
        this.slService.addIngredients(ingredients)
    }

    getRecipe(index: number){
        return this.recipes.slice()[index];
    }

    addRecipe(recipe: Recipe){
        this.recipes.push(recipe);
        this.recipesChanged.next(this.recipes.slice());
    }

    updateRecipe(index:number, newRecipe:Recipe){
        this.recipes[index] = newRecipe;
        this.recipesChanged.next(this.recipes.slice());
    }

    deleteRecipe(index: number){
        this.recipes.splice(index,1);
        this.recipesChanged.next(this.recipes.slice());
    }

}