import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { DataStorageService } from 'src/app/shared/data-storage.service';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  // @Output() showSection = new EventEmitter<string>();

  constructor(private dataService: DataStorageService,
    private authService: AuthService) { }

  ngOnInit() {
  }

  onSaveData(){
    this.dataService.storeRecipes()
    .subscribe(
      (response: Response) => {
        console.log(response);
      },
      (error) => console.log(error)
    );
  }

  onFetchData(){
    this.dataService.fetchRecipes();
  }

  onLogout(){
    this.authService.logout();
  }

  // onHeaderClick(section: string){
  //   console.log("clicked header " + section + " option");
  //   this.showSection.emit(section);
  // }

isAuthenticated(){
  return this.authService.isAuthenticated();
}

}
