import { Component, OnInit, OnDestroy, ViewChild, 
  // ViewChild, ElementRef
} from '@angular/core';
import { Ingredient } from '../../shared/ingredient.module';
import { shoppingListService } from '../shopping-list.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { typeWithParameters } from '@angular/compiler/src/render3/util';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';


@Component({
  selector: 'app-shopping-editor',
  templateUrl: './shopping-editor.component.html',
  styleUrls: ['./shopping-editor.component.css']
})
export class ShoppingEditorComponent implements OnInit, OnDestroy {
  // @ViewChild('nameInput') nameInput: ElementRef;
  // @ViewChild('amountInput') amountInput: ElementRef;
  // @Output() newItem = new EventEmitter<Ingredient>();

  @ViewChild('f') slForm: NgForm;

  subscription: Subscription;
  editMode= false;
  editedItemIndex: number;
  editedItem: Ingredient;

  constructor(private shopService: shoppingListService) { }

  ngOnInit() {
    this.subscription = this.shopService.startedEditing
      .subscribe(
        (index: number) => {
          this.editMode = true;
          this.editedItemIndex = index;
          this.editedItem = this.shopService.getIngredient(index);
          this.slForm.setValue({
            name: this.editedItem.name,
            amount: this.editedItem.amount
          })
        }
      );

  }

  onSubmit(form: NgForm){
    const value = form.value;
    const newIngredient = new Ingredient(value.name,value.amount);
    if(this.editMode){
      this.shopService.updateIngredient(this.editedItemIndex, newIngredient);
    }else{
      this.shopService.addIngredient(newIngredient);
    }
    this.editMode=false;
    form.reset();

    // console.log("name: " +  this.nameInput.nativeElement.value);
    // console.log("amount: " +  this.amountInput.nativeElement.value)
    // // this.newItem.emit(new Ingredient(this.nameInput.nativeElement.value,
    // //   this.amountInput.nativeElement.value ))
    // this.shopService.addIngredient(new Ingredient(this.nameInput.nativeElement.value, this.amountInput.nativeElement.value ));

  }

  onClear(){
    this.slForm.reset();
    this.editMode=false;
  }

  onDelete(form : NgForm){
    this.shopService.deleteIngredient(this.editedItemIndex);
    this.onClear();


  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
