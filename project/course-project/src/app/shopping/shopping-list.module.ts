import { NgModule } from "@angular/core";
import { ShoppingListComponent } from "./shopping-list.component";
import { ShoppingEditorComponent } from "./shopping-editor/shopping-editor.component";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

@NgModule({
    declarations: [
        ShoppingListComponent,
        ShoppingEditorComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
    ],
})
export class ShoppingModule {}