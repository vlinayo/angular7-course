import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { map, switchMap, mergeMap, tap } from 'rxjs/operators';
import { from } from 'rxjs';
import { Router } from '@angular/router';

import * as fireBase from 'firebase';
import * as AuthActions from './auth.actions';


@Injectable()
export class AuthEffects {
    @Effect()
    authSignin = this.actions$
        .ofType(AuthActions.TRY_SIGNIN)
        .pipe(map(
            (action: AuthActions.TrySignIn) => {
                return action.payload;
            }
        ),
        switchMap(
            (authData: {username: string, password: string}) => {
                return from(fireBase.auth().signInWithEmailAndPassword(
                    authData.username,
                    authData.password))
            }
        ),
        switchMap(
            () => {
                return from(fireBase.auth().currentUser.getIdToken());
            }
        ),
        mergeMap(
            (token: string) => {
                this.router.navigate(['/']);
                return [
                    {
                        type: AuthActions.SIGNIN
                    },
                    {
                        type: AuthActions.SET_TOKEN,
                        payload: token
                    }
                ]
            }
        ))


    @Effect()
    authSignup = this.actions$
        .ofType(AuthActions.TRY_SIGNUP)
        .pipe(map(
            (action: AuthActions.TrySignUp) => {
                return action.payload;
            }
        ),
        switchMap(
            (authData: {username:string, password:string}) => {
              return from(fireBase.auth().
                createUserWithEmailAndPassword(
                    authData.username, 
                    authData.password))
            }
        ),
        switchMap(
            () => {
                return from(fireBase.auth().currentUser.getIdToken());
            }
        ),
        mergeMap(
            (token: string) => {
            return [
                {
                    type: AuthActions.SIGNUP
                },
                {
                    type: AuthActions.SET_TOKEN,
                    payload: token
                }
            ];
        }));

    @Effect({dispatch: false})
    authoLogout = this.actions$
        .ofType(AuthActions.LOGOUT)
        .pipe(tap( () => {
            this.router.navigate(['/signin']);
        }));

    constructor(private actions$: Actions, private router: Router){

    }
}