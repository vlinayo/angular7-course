import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad } from "@angular/router";
import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { map, take } from 'rxjs/operators';

import * as fromApp from '../state/app.reducers';
import * as fromAuth from './state/auth.reducers';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {
    
    constructor(private store: Store<fromApp.AppState>){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        return this.store.select('auth').pipe(
            take(1),
            map(
            (authState: fromAuth.State) => {
                return authState.authenticated;
            }
        ));
    }

    canLoad(){
        return this.store.select('auth').pipe(
            take(1),
            map(
            (authState: fromAuth.State) => {
                return authState.authenticated;
            }
        ));
    }
}