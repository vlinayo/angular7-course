import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ShoppingListComponent } from "./shopping/shopping-list.component";
import { AuthGuard } from "./auth/auth-guard.service";
import { HomeComponent } from "./core/home/home.component";

const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    { path: 'recipes', loadChildren: './recipe-book/recipe-book.module#RecipeModule', canLoad: [AuthGuard]},
    {path: 'shopping-list', component: ShoppingListComponent, canActivate: [AuthGuard]},    
];

@NgModule({
    //DEFAULT PRELOAD: "Dont preload"
    imports: [RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})],
    exports: [RouterModule]
})
export class AppRoutingModule {

}