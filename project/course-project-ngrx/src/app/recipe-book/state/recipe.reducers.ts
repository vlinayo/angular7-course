import { Recipe } from "../recipe.model";
import { Ingredient } from "src/app/shared/ingredient.module";
import * as RecipeActions from "./recipe.actions";
import * as fromApp from '../../state/app.reducers';

export interface FeatureState extends fromApp.AppState {
    recipes: State
}

export interface State {
    recipes: Recipe[]
 }
 
 const initialState: State = {
    recipes: [
        new Recipe('Pizza!',
        'This is a pizza recipe',
        'https://img.bestrecipes.com.au/RCK3slSo/h300-w400-cscale/br-api/asset/20771/super-easy-pizza-dough-recipe.jpg',
        [
            new Ingredient('Cheeese',5),
            new Ingredient('Tomatoes',6),
            new Ingredient('Flour',8)
        ]),

        new Recipe('Tortilla de Patatas',
        'Asi se hace!',
        'https://cdn1.cocina-familiar.com/recetas/thumb/tortilla-de-patata-con-cebolla.jpg',
        [
            new Ingredient('Potatoes', 4),
            new Ingredient('Onions', 2),
            new Ingredient('Eggs', 6)
        ])
    ]
 }

export function recipeReducer(state = initialState, action: RecipeActions.RecipeActions ){
    switch(action.type){
        case RecipeActions.SET_RECIPES:
            return {
                ...state,
                recipes: [...action.payload]
            };
        case RecipeActions.ADD_RECIPE:
            return {
                ...state,
                recipes: [...state.recipes, action.payload]
            };
        case RecipeActions.UPDATE_RECIPE:
            const recipe = state.recipes[action.payload.index];
            const updatedRecipe = {
                ...recipe,
                ...action.payload.updatedRec
            };
            const recipes = [...state.recipes];
            recipes[action.payload.index] = updatedRecipe;
            return {
                ...state,
                recipes: recipes

            };
        case RecipeActions.DELETE_RECIPE:
            const afterDeleteRecipes = [...state.recipes];
            afterDeleteRecipes.splice(action.payload, 1);        
            return {
                ...state,
                recipes: afterDeleteRecipes
            };
        default:
            return state;
    }
}