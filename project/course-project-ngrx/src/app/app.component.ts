import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  // title = 'course-proyect';
  displaySection = 'recipe'
  
  onSectionSelected(selection : string){
    this.displaySection = selection;
  }

  ngOnInit(): void {
    firebase.initializeApp({
      apiKey: "AIzaSyCCS7SD__DGDClBcYngsalpLTkt4CdVeXg",
      authDomain: "udemy-proyect-production.firebaseapp.com"
    });
  }
}
