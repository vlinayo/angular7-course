import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { ShoppingListComponent } from "./shopping-list.component";
import { ShoppingEditorComponent } from "./shopping-editor/shopping-editor.component";

@NgModule({
    declarations: [
        ShoppingListComponent,
        ShoppingEditorComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
    ],
})
export class ShoppingModule {}