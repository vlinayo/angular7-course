import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredient.module';
import { Observable } from 'rxjs';

import { Store } from '@ngrx/store';
import * as ShoppingListActions from './state/shopping-list.actions';
import * as fromApp from '../state/app.reducers';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  
  shoppingListState: Observable<{ingredients: Ingredient[]}>;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.shoppingListState = this.store.select('shoppingList');
  }

  onEditItem(index: number){
    this.store.dispatch(new ShoppingListActions.StartEdit(index));
  }

}
