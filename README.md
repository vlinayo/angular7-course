# Angular 7 - Course

## Anotaciones Importantes en la realización del Curso:

Firebase Urls: 
* https://udemy-ng-http-68079.firebaseio.com/
* https://udemy-ng-http-68079.firebaseio.com/appName
* Project: https://udemy-ng-project-a07e2.firebaseio.com/
* Published: https://udemy-proyect-production.firebaseapp.com/

Problemas posibles con rxjs:
necesidad de usar: rxjs-compat (npm install --save rxjs-compat)

¿Qué cambia?
la funcion **map**: pipe(map(...)), en el catch del error y los imports:

    * import 'rxjs/Rx';
    * import { Observable } from 'rxjs/Observable'; 
    * ...catch(error => {
            return Observable.throw(...)
        }) 

Quedando:

    * ...pipe(catchError(error => {
            return throwError(...)
        }))

imports:
    * import { catchError } from 'rxjs/operators'; 
    * import { throwError } from 'rxjs'; 


-------------------------------
 ### Firebase  

_If you're using Firebase 5.x or higher (you can check the package.json  file to find out), 
you should use getIdToken()  for obtaining the token, NOT getToken()  as shown in the next lectures._

**Recordar** que para hacer un build se ejcuta:
_**ng build --prod --aot**_