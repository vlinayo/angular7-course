import { PipeTransform, Pipe } from "@angular/core";

@Pipe({
    name: 'shorten'
})
export class ShortenPipe implements PipeTransform {
    transform(inputValue: any, limit: number) {
        if(inputValue.length > limit){
            return inputValue.substr(0,limit) + '...';
        }else{
            return inputValue;
        }
    }
}