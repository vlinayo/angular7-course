import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {

  transform(value: string): any {
    //   let reversedVal = '';
    //   for(var i of value){
    //     reversedVal = i + reversedVal;
    //   }

    // return reversedVal;

    //mas sencillo:
    return value.split('').reverse().join('');
  }

}